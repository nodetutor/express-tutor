import * as express from 'express';

var app = express();
var port = 8081;

app.get('/', function (req, res) {
  res.send('Hello Express!')
})

app.listen(port, function () {
  console.log(`Express app listening on port ${port}!`)
})
