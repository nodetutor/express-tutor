# Express tutorial

## Setting Up Node Dev Environement

_Platform dependent_

On Ubuntu,

    sudo apt install -y nodejs


## Setting Up Express

    npm i -g express-generator

    npm i -S express
    npm i --save-dev @types/express





_tbd_


## References

* [Express web framework (Node.js/JavaScript)](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs)
* [Microsoft/TypeScript-Node-Starter](https://github.com/Microsoft/TypeScript-Node-Starter)
* [TypeScript 2 + Express + Node.js](http://brianflove.com/2016/11/08/typescript-2-express-node/)
* []()

