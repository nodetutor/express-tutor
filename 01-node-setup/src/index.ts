// Load HTTP module
import * as http from 'http';


// Create HTTP server and listen for requests
var port = 8080
var server = http.createServer((request, response) => {
   // Set the response HTTP header with HTTP status and Content type
   response.writeHead(200, {'Content-Type': 'text/plain'});
   
   // Send the response body "Hello World"
   response.end('Hello World\n');
});
server.listen(port);

// Print URL for accessing server
console.log(`Server running at http://localhost:${port}/`);
